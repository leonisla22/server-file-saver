const express = require("express");
const fileUpload = require("express-fileupload");
const cors = require("cors");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");

const app = express();

// enable files upload
app.use(
  fileUpload({
    createParentPath: true,
  })
);

//add other middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan("dev"));

//start app
const port = 8081;

app.listen(port, () => console.log(`App is listening on port ${port}.`));

app.get("*", async (req, res) => {
  try {
    res.status(200).send("ok");
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

app.post("*", async (req, res) => {
  try {
    if (!req.files) {
      res.send({
        status: false,
        message: "No file uploaded",
      });
    } else {
      const provisional = req.files.provisional;
      const name = provisional.name.split(".tar.gz")[0];
      const basePath = "./uploads/" + Date.now() + "_" + name;
      provisional.mv(basePath + ".tar.gz");
      fs.writeFileSync(
        path.join(__dirname, basePath + ".json"),
        JSON.stringify({ ...req.body, hash: provisional.md5 })
      );
      res.send({ url: "localhost:8080" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});
